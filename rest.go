package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
)

// RegContacto is ...
type RegContacto struct {
	IDContacto     int     `json:"id"`
	NombreCompleto string  `json:"nom"`
	Email          string  `json:"email"`
	TelMovil       string  `json:"telefono"`
	FechaCaptura   string  `json:"fecha"`
	EstReg         *EstReg `json:"estado"`
}

// EstReg is ...
type EstReg struct {
	idEstado  int    `json:"idestado"`
	nombreest string `json:"nombrestado"`
}

var (
	contactos []RegContacto
	bandera   = false
)

func main() {
	fmt.Println("Iniciamos con éxito")

	contactos = append(contactos, RegContacto{IDContacto: 1, NombreCompleto: "Alexander", Email: "alexanderacha@gmail.com", TelMovil: "5555", FechaCaptura: "1999/01/11", EstReg: &EstReg{idEstado: 1, nombreest: "Michoácan"}})
	contactos = append(contactos, RegContacto{IDContacto: 2, NombreCompleto: "Alexander2", Email: "alexanderacha@gmail.com2", TelMovil: "25555", FechaCaptura: "1999/01/02", EstReg: &EstReg{idEstado: 2, nombreest: "2Michoácan"}})
	contactos = append(contactos, RegContacto{IDContacto: 3, NombreCompleto: "Carlos V", Email: "carlosV@gmail.com", TelMovil: "5555-V", FechaCaptura: "2020/05/05", EstReg: &EstReg{idEstado: 5, nombreest: "EstadoV"}})

	router := mux.NewRouter()
	router.HandleFunc("/", LandingPage).Methods("GET")
	router.HandleFunc("/registros", GetregistrosEndpoint).Methods("GET")
	router.HandleFunc("/registros/{id}", GetRegistroEndpoint).Methods("GET")
	router.HandleFunc("/contactosreg", PostRegistroEndpoint).Methods("POST")

	handlerCORS := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowCredentials: true,
	}).Handler(router)

	http.ListenAndServe("localhost:3030", handlerCORS)

}

// LandingPage is ...
func LandingPage(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Hecho!")
	fmt.Fprintf(w, "éxito!")
	contador := 0
	var con int
	if w != nil {
		contador++
		con += contador + 1
	} else {
		con = contador
	}
	fmt.Println(con)
}

// GetregistrosEndpoint is ...
func GetregistrosEndpoint(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Consulta Registros!")
	json.NewEncoder(w).Encode(contactos)
}

//GetRegistroEndpoint is ...
func GetRegistroEndpoint(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	indice, _ := strconv.Atoi(params["id"])

	json.NewEncoder(w).Encode(contactos[indice])
}

//PostRegistroEndpoint is ...
func PostRegistroEndpoint(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Hecho POST")
	var nuevo RegContacto
	json.NewDecoder(r.Body).Decode(&nuevo)

	contactos = append(contactos, nuevo)

	json.NewEncoder(w).Encode(contactos)
}
